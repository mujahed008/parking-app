drop database parkingapp;
create database parkingapp;
use parkingapp;

CREATE TABLE IF NOT EXISTS application (
  row_id bigint(20) NOT NULL AUTO_INCREMENT,
  created_on datetime NOT NULL,
  last_modified_on datetime NOT NULL,
  deleted tinyint(1) DEFAULT 0,
  app_name varchar(50) NOT NULL UNIQUE,
  PRIMARY KEY (row_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS user (
  row_id bigint(20) NOT NULL AUTO_INCREMENT,
  created_on datetime NOT NULL,
  last_modified_on datetime NOT NULL,
  deleted tinyint(1) DEFAULT 0,
  username varchar(50) NOT NULL UNIQUE,
  password varchar(512) NOT NULL,
  name varchar(50) NOT NULL,
  email varchar(50) DEFAULT NULL,
  phone varchar(15) DEFAULT NULL,
  PRIMARY KEY (row_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS user__application (    
  user_id bigint(20) NOT NULL,
  application_id bigint(20) NOT NULL,
  PRIMARY KEY (user_id),
  KEY fk_user__application___user (user_id),
  CONSTRAINT fk_user__application___user FOREIGN KEY (user_id) REFERENCES user (row_id),
  KEY fk_user__application___application (application_id),
  CONSTRAINT fk_user__application___application FOREIGN KEY (application_id) REFERENCES application (row_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS role (
  row_id bigint(20) NOT NULL AUTO_INCREMENT,
  created_on datetime NOT NULL,
  last_modified_on datetime NOT NULL,
  deleted tinyint(1) DEFAULT 0,
  role varchar(50) NOT NULL UNIQUE,
  PRIMARY KEY (row_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS user_role (
  user_id bigint(20) NOT NULL,  
  role_id bigint(20) NOT NULL,
  PRIMARY KEY (user_id, role_id),
  KEY fk_user_role__user (user_id),
  CONSTRAINT fk_user_role__user FOREIGN KEY (user_id) REFERENCES user (row_id),
  KEY fk_user_role__role (role_id),
  CONSTRAINT fk_user_role__role FOREIGN KEY (role_id) REFERENCES role (row_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci;


INSERT INTO `drapps2backend`.`role`
(`row_id`, `created_on`, `last_modified_on`, `deleted`, `role`) VALUES (4, now(), now(), 0, 'ROLE_NORMAL');
INSERT INTO `drapps2backend`.`role`
(`row_id`, `created_on`, `last_modified_on`, `deleted`, `role`) VALUES (2, now(), now(), 0, 'ROLE_SA');
INSERT INTO `drapps2backend`.`role`
(`row_id`, `created_on`, `last_modified_on`, `deleted`, `role`) VALUES (3, now(), now(), 0, 'ROLE_CASHER');
INSERT INTO `drapps2backend`.`role`
(`row_id`, `created_on`, `last_modified_on`, `deleted`, `role`) VALUES (1, now(), now(), 0, 'ROLE_OWNER');

INSERT INTO application (row_id, created_on, last_modified_on, deleted, app_name) VALUES 
(1, now(), now(), 0, 'SA-App');

INSERT INTO user (row_id, created_on, last_modified_on, deleted, username, password, name, email, phone) VALUES
(1, now(), now(), 0, 'sa', 'sa99', 'Super Admin', 'info@parking.com', null);

INSERT INTO user_role (user_id, role_id) VALUES
(1, 1);

CREATE TABLE IF NOT EXISTS capacity (
  row_id bigint(20) NOT NULL AUTO_INCREMENT,
  created_on datetime NOT NULL,
  last_modified_on datetime NOT NULL,
  deleted tinyint(1) DEFAULT 0,
  limit int(11) NOT NULL,
  latitude double,
  longitude double,
  location_name varchar(100) NULL defalut NULL;
  PRIMARY KEY (row_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS capacity__user (    
  capacity_id bigint(20) NOT NULL,
  user_id bigint(20) NOT NULL,
  PRIMARY KEY (capacity_id),
  KEY fk_capacity__user___capacity (capacity_id),
  CONSTRAINT fk_capacity__user___capacity FOREIGN KEY (capacity_id) REFERENCES capacity (row_id),
  KEY fk_capacity__user___user (user_id),
  CONSTRAINT fk_capacity__user___user FOREIGN KEY (user_id) REFERENCES user (row_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS vehicle_type (
  row_id bigint(20) NOT NULL AUTO_INCREMENT,
  created_on datetime NOT NULL,
  last_modified_on datetime NOT NULL,
  deleted tinyint(1) DEFAULT 0,
  type varchar(20) NOT NULL,
  PRIMARY KEY (row_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci;



CREATE TABLE IF NOT EXISTS price_list (
  row_id bigint(20) NOT NULL AUTO_INCREMENT,
  created_on datetime NOT NULL,
  last_modified_on datetime NOT NULL,
  deleted tinyint(1) DEFAULT 0,
  day ENUM('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday') NOT NULL
  frm_hours int(11) NOT NULL,
  to_hours int(11) NOT NULL,
  charges double NOT NULL,
  PRIMARY KEY (row_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS price_list__vehicle_type (
  price_id bigint(20) NOT NULL,  
  v_type_id bigint(20) NOT NULL,
  PRIMARY KEY (price_id),
  KEY fk_price_list__vehicle_type___price_list (price_id),
  CONSTRAINT fk_price_list__vehicle_type___price_list FOREIGN KEY (price_id) REFERENCES price_list (row_id),
  KEY fk_price_list__vehicle_type___vehicle_type (v_type_id),
  CONSTRAINT fk_price_list__vehicle_type___vehicle_type FOREIGN KEY (v_type_id) REFERENCES vehicle_type (row_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS price_list__capacity (    
  price_id bigint(20) NOT NULL,
  capacity_id bigint(20) NOT NULL,
  PRIMARY KEY (price_id),
  KEY fk_price_list__capacity___price_list (price_id),
  CONSTRAINT fk_price_list__capacity___price_list FOREIGN KEY (price_id) REFERENCES price_list (row_id),
  KEY fk_price_list__capacity___capacity (capacity_id),
  CONSTRAINT fk_price_list__capacity___capacity FOREIGN KEY (capacity_id) REFERENCES capacity (row_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci;




