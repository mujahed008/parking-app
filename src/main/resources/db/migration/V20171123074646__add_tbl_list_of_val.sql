CREATE TABLE IF NOT EXISTS list_of_value (
  row_id bigint(20) NOT NULL AUTO_INCREMENT,
  created_on datetime NULL DEFAULT NULL,
  last_modified_on datetime NULL DEFAULT NULL,
  deleted tinyint(1) DEFAULT 0,
  display varchar(255) NOT NULL,
  val varchar(255) NOT NULL,  
  type varchar(20) NOT NULL,
  parent_type varchar(20) NULL DEFAULT NULL,
  sequence_no bigint(20) NULL DEFAULT NULL,
  deletable tinyint(1) NOT NULL DEFAULT 1,  
  application_id bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (row_id),
  KEY fk_list_of_value__application (application_id),
  CONSTRAINT fk_list_of_value__application FOREIGN KEY (application_id) REFERENCES application (row_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci;