package com.parking.dao;

import com.parking.model.EntityIdRepository;

public interface EntityIdRepositoryDao {

	public EntityIdRepository getEntityIdRepository(Long rowId, Long applicationId);
	
	public EntityIdRepository getEntityIdRepository(String entityName, Long applicationId);
}
