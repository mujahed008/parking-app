package com.parking.dao;

import java.util.Set;

import com.parking.model.Role;

public interface RoleDao {

	Set<Role> findRoles(String... roles);
}