package com.parking.dao;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;

import com.parking.model.EntityIdRepository;
import com.parking.repository.EntityIdRepositoryRepository;

@Repository
public class EntityIdRepositoryDaoImpl implements EntityIdRepositoryDao{
	
	@Autowired
	private EntityIdRepositoryRepository entityIdRepository;

	@Override
	public EntityIdRepository getEntityIdRepository(Long rowId, Long applicationId) {
		EntityIdRepository entity = entityIdRepository.findOne(new Specification<EntityIdRepository>() {
			@Override
			public Predicate toPredicate(Root<EntityIdRepository> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				cq.where(cb.and(cb.equal(root.get("rowId"), rowId)), cb.and(cb.equal(root.get("application").get("rowId"), applicationId)));
				return null;
			}
		});
		return entity;
	}

	@Override
	public EntityIdRepository getEntityIdRepository(String entityName, Long applicationId) {
		EntityIdRepository entity = entityIdRepository.findOne(new Specification<EntityIdRepository>() {
			@Override
			public Predicate toPredicate(Root<EntityIdRepository> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				cq.where(cb.and(cb.equal(root.get("entityName"), entityName)), cb.and(cb.equal(root.get("application").get("rowId"), applicationId)));
				return null;
			}
		});
		return entity;
	}

}
