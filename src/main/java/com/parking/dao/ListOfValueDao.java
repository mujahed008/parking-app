package com.parking.dao;

import java.util.List;

public interface ListOfValueDao {
	
	public int softDelete(List<Long> rowIds);

}
