package com.parking.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class PersistanceDaoImpl implements PersistanceDao{

	@Autowired
	private EntityManager em;
	
	@Override
	@Transactional
	public <T> void softDelete(Class<T> clazz, Long rowId) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaUpdate<T> cu = cb.createCriteriaUpdate(clazz);
		Root<T> root = cu.from(clazz);
        cu.set(root.get("deleted"), true);
        cu.set(root.get("modified"), new Date());
        cu.where(cb.equal(root.get("rowId"), rowId));
        /*EntityTransaction txn = em.getTransaction();
        txn.begin();*/
        em.createQuery(cu).executeUpdate();
        /*txn.commit();*/
	}


	@Override
	@Transactional
	public <T> void softDelete(Class<T> clazz, List<Long> rowIds) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaUpdate<T> cu = cb.createCriteriaUpdate(clazz);
		Root<T> root = cu.from(clazz);
        cu.set(root.get("deleted"), true);
        cu.set(root.get("modified"), new Date());
        cu.where(cb.in(root.get("rowId")).value(rowIds));
        em.createQuery(cu).executeUpdate();
	}


	@Override
	@Transactional
	public int deleteAppCreateMigrations() {
		Query query = em.createNativeQuery("delete from schema_version where script like '%_APP_CREATE_%'");
		int r = query.executeUpdate();
		return r;
	}
}
