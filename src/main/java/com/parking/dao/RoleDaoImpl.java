package com.parking.dao;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;

import com.parking.model.Role;
import com.parking.repository.RoleRepository;

@Repository
public class RoleDaoImpl implements RoleDao{

	@Autowired
	private RoleRepository roleRepository;

	@Override
	public Set<Role> findRoles(String... roles) {
		List<Role> roleList = roleRepository.findAll(new Specification<Role>() {
			@Override
			public Predicate toPredicate(Root<Role> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				cq.where(cb.in(root.get("role")).value(Arrays.asList(roles)));
				return null;
			}
		});
		return new HashSet<>(roleList);
	}
	
	
}
