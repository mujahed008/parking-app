package com.parking.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.parking.model.ListOfValue;

@Repository
public class ListOfValueDaoImpl implements ListOfValueDao {
	
	@Autowired
	private EntityManager em;

	@Override
	@Transactional
	public int softDelete(List<Long> rowIds) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaUpdate<ListOfValue> cu = cb.createCriteriaUpdate(ListOfValue.class);
		Root<ListOfValue> root = cu.from(ListOfValue.class);
        cu.set(root.get("deleted"), true);
        cu.set(root.get("modified"), new Date());
        cu.where(cb.and(cb.equal(root.get("deletable"), true), cb.in(root.get("rowId")).value(rowIds)));
        int status = em.createQuery(cu).executeUpdate();
		return status;
	}

}
