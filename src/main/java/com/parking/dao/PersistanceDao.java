package com.parking.dao;

import java.util.List;

public interface PersistanceDao {

	public <T> void softDelete(Class<T> clazz, Long rowId);
	
	public <T> void softDelete(Class<T> clazz, List<Long> rowIds);
	
	public int deleteAppCreateMigrations();
}
