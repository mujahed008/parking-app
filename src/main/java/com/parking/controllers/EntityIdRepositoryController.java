package com.parking.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.parking.commons.AppConstants;
import com.parking.forms.EntityIdRepositoryForm;
import com.parking.pojo.DeleteDto;
import com.parking.pojo.EntityIdRepositoryDto;
import com.parking.pojo.EntityIdRepositoryPaginatedList;
import com.parking.services.EntityIdRepositoryService;

@Controller
public class EntityIdRepositoryController {
	
	@Autowired
	private EntityIdRepositoryService entityIdRepositoryService;
	
	@GetMapping("/entityIdRepository/list")
	public String renderEntityIdRepository(Model model, @RequestParam(required = false, defaultValue=AppConstants._PAGE) Integer page, @RequestParam(required = false, defaultValue=AppConstants._SIZE) Integer size, @RequestParam(required = false, defaultValue=AppConstants._MODIFIED) String orderBy){
		EntityIdRepositoryPaginatedList paginatedList = entityIdRepositoryService.getEntityIdRepositoryPaginatedList(page, size, orderBy);
		model.addAttribute("paginatedList", paginatedList);
		return "views/entity-id-repository-list";
	}
	
	@GetMapping("/entityIdRepository/create")
	public String renderEntityIdRepositoryCreate(@ModelAttribute("entityIdRepositoryForm") EntityIdRepositoryForm entityIdRepositoryForm, Model model){
		return "views/entity-id-repository-create";
	}
	
	
	@PostMapping("/entityIdRepository/create")
	public String createEntityIdRepositoryCreate(@ModelAttribute("entityIdRepositoryForm") @Validated EntityIdRepositoryForm entityIdRepositoryForm, BindingResult bindingResult, Model model){
		if(bindingResult.hasErrors()){
			return "views/entity-id-repository-create";
		}
		EntityIdRepositoryDto entityDto = entityIdRepositoryService.createEntityIdRepository(entityIdRepositoryForm);
		model.addAttribute("entityDto", entityDto);
		return "views/entity-id-repository-details";
	}
	
	@GetMapping("/entityIdRepository/{rowId}/details")
	public String renderEntityIdRepositoryDetails(Model model, @PathVariable Long rowId){
		EntityIdRepositoryDto entityDto =  entityIdRepositoryService.getEntityIdRepository(rowId);
		model.addAttribute("entityDto", entityDto);
		return "views/entity-id-repository-details";
	}
	
	@GetMapping("/entityIdRepository/{rowId}/edit")
	public String renderEntityIdRepositoryEdit(Model model, @PathVariable Long rowId){
		EntityIdRepositoryForm entityForm =  entityIdRepositoryService.getEntityIdRepositoryForm(rowId);
		model.addAttribute("entityIdRepositoryForm", entityForm);
		return "views/entity-id-repository-edit";
	}
	
	@PostMapping("/entityIdRepository/edit")
	public String editEntityIdRepository(@ModelAttribute("entityIdRepositoryForm") @Validated EntityIdRepositoryForm entityIdRepositoryForm, BindingResult bindingResult, Model model){
		if(bindingResult.hasErrors()){
			return "views/entity-id-repository-edit";
		}
		EntityIdRepositoryDto entityDto =  entityIdRepositoryService.editEntityIdRepository(entityIdRepositoryForm);
		model.addAttribute("entityDto", entityDto);
		return "views/entity-id-repository-details";
	}
	
	@PostMapping("/entityIdRepository/delete")
	public ResponseEntity<HttpStatus> delete(DeleteDto deleteDto, Model model){
		entityIdRepositoryService.deleteRecords(deleteDto.getToDelete());
		return new ResponseEntity<HttpStatus>(HttpStatus.OK);
	}
}