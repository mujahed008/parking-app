package com.parking.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.parking.pojo.AuthRequest;
import com.parking.pojo.AuthResponse;
import com.parking.security.TokenUtils;
import com.parking.security.UserDetailsImpl;
import com.parking.services.AuthService;

@Controller
public class AuthController {
	
	@Autowired
	private AuthService authService;
	
	@Autowired
	private TokenUtils tokenUtils;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@RequestMapping(value="/auth/login", method=RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public @ResponseBody AuthResponse login(@RequestBody AuthRequest request, Device device){
		final Authentication authentication = this.authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(),
						request.getPassword()
		));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		final UserDetailsImpl secureUser = (UserDetailsImpl) authentication.getPrincipal();
		
		final String token = this.tokenUtils.generateObjectToken(secureUser, device);
		AuthResponse response = new AuthResponse();
		response.setAuthToken(token);
		response.setName(secureUser.getUser().getName());
		return response;
	}
}
