package com.parking.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.parking.commons.AppConstants;
import com.parking.pojo.DeleteDto;
import com.parking.pojo.ListOfValueDto;
import com.parking.pojo.ListOfValuePaginatedList;
import com.parking.services.ListOfValueService;

@Controller
public class ListOfValueController {

	@Autowired
	private ListOfValueService listOfValueService;

	@GetMapping("/listOfValue/list")
	public String renderListOfValueList(Model model, @RequestParam(required = false, defaultValue=AppConstants._PAGE) Integer page, @RequestParam(required = false, defaultValue=AppConstants._SIZE) Integer size, @RequestParam(required = false, defaultValue=AppConstants._MODIFIED) String orderBy){
		ListOfValuePaginatedList paginatedList = listOfValueService.getListOfValuePaginatedList(page, size, orderBy);
		model.addAttribute("paginatedList", paginatedList);
		return "views/list-of-value-list";
	}
	
	@GetMapping("/listOfValue/create")
	public String renderListOfValueCreate(@ModelAttribute("listOfValueDto") ListOfValueDto listOfValueDto, Model model){
		return "views/list-of-value-create";
	}
	
	
	@PostMapping("/listOfValue/create")
	public String createListOfValueCreate(@ModelAttribute("listOfValueDto") @Validated ListOfValueDto listOfValueDto, BindingResult bindingResult, Model model){
		if(bindingResult.hasErrors()){
			return "views/list-of-value-create";
		}
		listOfValueDto = listOfValueService.createListOfValue(listOfValueDto);
		model.addAttribute("listOfValueDto", listOfValueDto);
		return "views/list-of-value-details";
	}
	
	@GetMapping("/listOfValue/{rowId}/details")
	public String renderListOfValueDetails(Model model, @PathVariable Long rowId){
		ListOfValueDto entityDto =  listOfValueService.getListOfValue(rowId);
		model.addAttribute("listOfValueDto", entityDto);
		return "views/list-of-value-details";
	}
	
	@GetMapping("/listOfValue/{rowId}/edit")
	public String renderListOfValueEdit(Model model, @PathVariable Long rowId){
		ListOfValueDto listOfValueDto =  listOfValueService.getListOfValue(rowId);
		model.addAttribute("listOfValueDto", listOfValueDto);
		return "views/list-of-value-edit";
	}
	
	@PostMapping("/listOfValue/edit")
	public String editListOfValue(@ModelAttribute("listOfValueDto") @Validated ListOfValueDto listOfValueDto, BindingResult bindingResult, Model model){
		if(bindingResult.hasErrors()){
			return "views/list-of-value-edit";
		}
		listOfValueDto = listOfValueService.editListOfValue(listOfValueDto);
		model.addAttribute("ListOfValueDto", listOfValueDto);
		return "views/list-of-value-details";
	}
	
	@PostMapping("/listOfValue/delete")
	public ResponseEntity<HttpStatus> delete(DeleteDto deleteDto, Model model){
		listOfValueService.deleteRecords(deleteDto.getToDelete());
		return new ResponseEntity<HttpStatus>(HttpStatus.OK);
	}
	
}
