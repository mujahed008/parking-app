package com.parking.services;

import com.parking.forms.SignupForm;

public interface AuthService {
	
	public void signup(SignupForm signupForm);

}
