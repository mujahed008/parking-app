package com.parking.services;

import java.util.List;

import org.springframework.data.domain.Page;

import com.parking.model.ListOfValue;
import com.parking.pojo.ListOfValueDto;
import com.parking.pojo.ListOfValuePaginatedList;

public interface ListOfValueService {

	public ListOfValueDto createListOfValue(ListOfValueDto ListOfValueDto);
	
	public ListOfValueDto editListOfValue(ListOfValueDto ListOfValueDto);
	
	public Page<ListOfValue> getListOfValueList(Integer page, Integer size, String orderBy);
	
	public ListOfValuePaginatedList getListOfValuePaginatedList(Integer page, Integer size, String orderBy);
	
	public ListOfValueDto getListOfValue(Long rowId);
	
	public void deleteRecords(List<Long> rowIds);
}
