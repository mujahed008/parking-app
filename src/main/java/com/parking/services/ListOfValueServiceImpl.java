package com.parking.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.parking.commons.AppConstants;
import com.parking.dao.ListOfValueDao;
import com.parking.generic.TemplateModelPaginationImpl;
import com.parking.model.Application;
import com.parking.model.ListOfValue;
import com.parking.pojo.ListOfValueDto;
import com.parking.pojo.ListOfValuePaginatedList;
import com.parking.repository.ListOfValueRepository;
import com.parking.security.UserDetailsImpl;

@Service
public class ListOfValueServiceImpl implements ListOfValueService{
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private ListOfValueRepository listOfValueRepository;
	
	@Autowired
	private ListOfValueDao listOfValueDao;

	@Override
	public ListOfValueDto createListOfValue(ListOfValueDto ListOfValueDto) {
		ListOfValue entity = modelMapper.map(ListOfValueDto, ListOfValue.class);
		UserDetailsImpl user = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		entity.setApplication(user.getUser().getApplication());
		if(StringUtils.isEmpty(entity.getParentType())){
			entity.setParentType(AppConstants.PARENT);
		}
		entity = listOfValueRepository.save(entity);
		ListOfValueDto = modelMapper.map(entity, ListOfValueDto.class);
		return ListOfValueDto;
	}

	@Override
	public ListOfValueDto editListOfValue(ListOfValueDto listOfValueDto) {
		UserDetailsImpl user = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Application application = user.getUser().getApplication();
		ListOfValue entity =  listOfValueRepository.findOneByRowIdAndApplication(listOfValueDto.getRowId(), application);
		entity.setDisplay(listOfValueDto.getDisplay());
		entity.setVal(listOfValueDto.getVal());
		entity.setType(listOfValueDto.getType());
		if(StringUtils.isEmpty(listOfValueDto.getParentType())){
			entity.setParentType(AppConstants.PARENT);
		}else{
			entity.setParentType(listOfValueDto.getParentType());
		}
		entity.setSequenceNo(listOfValueDto.getSequenceNo());
		listOfValueRepository.save(entity);
		ListOfValueDto entityDto = modelMapper.map(entity, ListOfValueDto.class);
		return entityDto;
	}

	@Override
	public Page<ListOfValue> getListOfValueList(Integer page, Integer size, String orderBy) {
		Direction order = null;
		if(orderBy.startsWith("-")){
			order = Direction.DESC;
			orderBy = orderBy.replace("-", "");
		}else {
			order = Direction.ASC;
		}
		Page<ListOfValue> pg = listOfValueRepository.findAll(new PageRequest(page - 1, size, order, orderBy));
		return pg;
	}

	@Override
	public ListOfValuePaginatedList getListOfValuePaginatedList(Integer page, Integer size, String orderBy) {
		Page<ListOfValue> pg = this.getListOfValueList(page, size, orderBy);
		List<ListOfValueDto> entityDtoList = new ArrayList<ListOfValueDto>();
		for(ListOfValue entity : pg.getContent()){
			ListOfValueDto entityDto = modelMapper.map(entity, ListOfValueDto.class);
			entityDto.setApplicationId(entity.getApplication().getRowId());
			entityDtoList.add(entityDto);
		}		
		ListOfValuePaginatedList listOfValuePaginatedList = new ListOfValuePaginatedList(); 
		listOfValuePaginatedList.setListOfValueDtoList(entityDtoList);
		listOfValuePaginatedList.setPagination(new TemplateModelPaginationImpl<ListOfValue>(pg, AppConstants.LIST_OF_VALUE_LIST_URL));
		return listOfValuePaginatedList;
	}

	@Override
	public ListOfValueDto getListOfValue(Long rowId) {
		UserDetailsImpl user = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Application application = user.getUser().getApplication();
		ListOfValue ListOfValue = listOfValueRepository.findOneByRowIdAndApplication(rowId, application);
		ListOfValueDto listOfValueDto = modelMapper.map(ListOfValue, ListOfValueDto.class);
		listOfValueDto.setApplicationId(ListOfValue.getApplication().getRowId());
		return listOfValueDto;
	}

	@Override
	public void deleteRecords(List<Long> rowIds) {
		listOfValueDao.softDelete(rowIds);
	}
}
