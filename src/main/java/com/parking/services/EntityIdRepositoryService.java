package com.parking.services;

import java.util.List;

import org.springframework.data.domain.Page;

import com.parking.forms.EntityIdRepositoryForm;
import com.parking.model.EntityIdRepository;
import com.parking.pojo.EntityIdRepositoryDto;
import com.parking.pojo.EntityIdRepositoryPaginatedList;

public interface EntityIdRepositoryService {

	public EntityIdRepositoryDto createEntityIdRepository(EntityIdRepositoryForm entityIdRepositoryForm);
	
	public EntityIdRepositoryDto editEntityIdRepository(EntityIdRepositoryForm entityIdRepositoryForm);
	
	public Page<EntityIdRepository> getEntityIdRepository(Integer page, Integer size, String orderBy);
	
	public EntityIdRepositoryPaginatedList getEntityIdRepositoryPaginatedList(Integer page, Integer size, String orderBy);
	
	public EntityIdRepositoryDto getEntityIdRepository(Long rowId);
	
	public EntityIdRepositoryForm getEntityIdRepositoryForm(Long rowId);
	
	public String getNextId(String entityName);
	
	public void deleteRecords(List<Long> rowIds);
}
