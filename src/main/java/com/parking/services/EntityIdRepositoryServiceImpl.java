package com.parking.services;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.parking.commons.AppConstants;
import com.parking.commons.Utils;
import com.parking.dao.EntityIdRepositoryDao;
import com.parking.dao.PersistanceDao;
import com.parking.forms.EntityIdRepositoryForm;
import com.parking.generic.TemplateModelPaginationImpl;
import com.parking.model.Application;
import com.parking.model.EntityIdRepository;
import com.parking.pojo.EntityIdRepositoryDto;
import com.parking.pojo.EntityIdRepositoryPaginatedList;
import com.parking.repository.EntityIdRepositoryRepository;
import com.parking.security.UserDetailsImpl;

@Service
public class EntityIdRepositoryServiceImpl implements EntityIdRepositoryService{

	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private EntityIdRepositoryRepository entityIdRepositoryRepo;
	
	@Autowired
	private EntityIdRepositoryDao entityIdRepositoryDao;
	
	@Autowired
	private PersistanceDao persistanceDao;
	
	@Override
	public EntityIdRepositoryDto createEntityIdRepository(EntityIdRepositoryForm entityIdRepositoryForm) {
		
		EntityIdRepository entity = modelMapper.map(entityIdRepositoryForm, EntityIdRepository.class);
		UserDetailsImpl user = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		entity.setApplication(user.getUser().getApplication());
		entity = entityIdRepositoryRepo.save(entity);
		
		EntityIdRepositoryDto entityDto = modelMapper.map(entity, EntityIdRepositoryDto.class);
		entityDto.setApplicationId(entity.getApplication().getRowId());
		return entityDto;
	}

	@Override
	public Page<EntityIdRepository> getEntityIdRepository(Integer page, Integer size, String orderBy) {
		Direction order = null;
		if(orderBy.startsWith("-")){
			order = Direction.DESC;
			orderBy = orderBy.replace("-", "");
		}else {
			order = Direction.ASC;
		}
		Page<EntityIdRepository> pg = entityIdRepositoryRepo.findAll(new PageRequest(page - 1, size, order, orderBy));
		return pg;
	}

	@Override
	public EntityIdRepositoryPaginatedList getEntityIdRepositoryPaginatedList(Integer page, Integer size, String orderBy) {
		Page<EntityIdRepository> pg = this.getEntityIdRepository(page, size, orderBy);
		List<EntityIdRepositoryDto> entityDtoList = new ArrayList<EntityIdRepositoryDto>();
		for(EntityIdRepository entity : pg.getContent()){
			EntityIdRepositoryDto entityDto = modelMapper.map(entity, EntityIdRepositoryDto.class);
			entityDto.setApplicationId(entity.getApplication().getRowId());
			entityDtoList.add(entityDto);
		}		
		EntityIdRepositoryPaginatedList entityPaginatedList = new EntityIdRepositoryPaginatedList(); 
		entityPaginatedList.setEntityDtoList(entityDtoList);
		entityPaginatedList.setPagination(new TemplateModelPaginationImpl<>(pg, AppConstants.ENTITY_ID_REPO_LIST_URL));
		return entityPaginatedList;
	}

	@Override
	public EntityIdRepositoryDto getEntityIdRepository(Long rowId) {
		
		UserDetailsImpl user = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Long applicationId = user.getUser().getApplication().getRowId();
		EntityIdRepository entity = entityIdRepositoryDao.getEntityIdRepository(rowId, applicationId);
		EntityIdRepositoryDto entityDto = modelMapper.map(entity, EntityIdRepositoryDto.class);
		entityDto.setApplicationId(entity.getApplication().getRowId());
		return entityDto;
	}

	@Override
	public void deleteRecords(List<Long> rowIds) {
		persistanceDao.softDelete(EntityIdRepository.class, rowIds);
	}

	@Override
	public EntityIdRepositoryDto editEntityIdRepository(EntityIdRepositoryForm entityIdRepositoryForm) {
		UserDetailsImpl user = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Long applicationId = user.getUser().getApplication().getRowId();
		EntityIdRepository entity =  entityIdRepositoryDao.getEntityIdRepository(entityIdRepositoryForm.getRowId(), applicationId);
		
		modelMapper.map(entityIdRepositoryForm, entity);
		entityIdRepositoryRepo.save(entity);
		
		EntityIdRepositoryDto entityDto = modelMapper.map(entity, EntityIdRepositoryDto.class);
		entityDto.setApplicationId(entity.getApplication().getRowId());
		return entityDto;
	}

	@Override
	public EntityIdRepositoryForm getEntityIdRepositoryForm(Long rowId) {
		UserDetailsImpl user = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Long applicationId = user.getUser().getApplication().getRowId();
		EntityIdRepository entity =  entityIdRepositoryDao.getEntityIdRepository(rowId, applicationId);
		EntityIdRepositoryForm entityForm = modelMapper.map(entity, EntityIdRepositoryForm.class);
		return entityForm;
	}

	@Override
	public String getNextId(String entityName) {
		UserDetailsImpl user = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Application application = user.getUser().getApplication();
		EntityIdRepository entity =  entityIdRepositoryRepo.findOneByEntityNameAndApplication(entityName, application);
		Long nextSrNo = entity.getLastSrNo() + 1;
		StringBuilder nextId = new StringBuilder();
		nextId.append(Utils.blankIfEmpty(entity.getPrefix())).append(nextSrNo).append(Utils.blankIfEmpty(entity.getSuffix()));
		entity.setLastSrNo(nextSrNo);
		entityIdRepositoryRepo.save(entity);
		return nextId.toString();
	}
}
