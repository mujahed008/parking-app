package com.parking.services;

import org.flywaydb.core.Flyway;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.parking.commons.AppConstants;
import com.parking.dao.PersistanceDao;
import com.parking.dao.RoleDao;
import com.parking.forms.SignupForm;
import com.parking.model.Application;
import com.parking.model.User;
import com.parking.repository.UserRepository;

@Service
public class AuthServiceImpl implements AuthService{
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleDao roleDao;
	
	@Autowired
	private PersistanceDao persistanceDao;
	
	@Autowired
	private Flyway flyway;

	@Override
	public void signup(SignupForm signupForm) {
		
		User user = modelMapper.map(signupForm, User.class);
		Application application = new Application();
		application.setAppName(user.getUsername().concat("_application"));
		application.setClinicName("Default Clinic");
		user.setApplication(application);
		user.setRoles(roleDao.findRoles(AppConstants._SIGNUP_ROLES));
		userRepository.save(user);
		persistanceDao.deleteAppCreateMigrations();
		flyway.migrate();
	}
}
