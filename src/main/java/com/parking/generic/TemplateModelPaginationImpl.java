package com.parking.generic;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;

public class TemplateModelPaginationImpl<T> implements Pagination<T>{
	
	private PaginationPage navPages[];
	private PaginationPage prevPage;
	private PaginationPage nextPage;
	
	public TemplateModelPaginationImpl(Page<T> page, String url){
		int current = page.getNumber() + 1;
		int last = page.getTotalPages();
		int delta = 2;
		int left = current - delta;
		int right = current + delta + 1;
		int l = -1;
		List<Integer> range = new ArrayList<Integer>();
		List<PaginationPage> rangeWithDots = new ArrayList<PaginationPage>();
		
		range.add(1);
		for(int i = current - delta; i <= current + delta; i++){
			if (i >= left && i < right && i < last && i > 1) {
	            range.add(i);
	        }
		}
		if(last != 1)
		range.add(last);
		for (int i : range){
			
			if(l != -1){				
				if(i - l == 2){
					if(l + 1 == current)
						rangeWithDots.add(new PaginationPage(url, l + 1, page.getSize(), true));
					else
						rangeWithDots.add(new PaginationPage(url, l + 1, page.getSize(), false));
				}else if(i - l != 1){
					rangeWithDots.add(new PaginationPage("..."));
				}
			}
			
			if(i == current)
				rangeWithDots.add(new PaginationPage(url, i, page.getSize(), true));
			else
				rangeWithDots.add(new PaginationPage(url, i, page.getSize(), false));
			
			l = i;
		}
		
		this.navPages = new PaginationPage[rangeWithDots.size()];
		rangeWithDots.toArray(this.navPages);
		
		if(current - 1 == 0){
			this.prevPage = new PaginationPage(true);
		}else {
			this.prevPage = new PaginationPage(url, current - 1, page.getSize(), false);
		}
		
		if(current == page.getTotalPages()){
			this.nextPage = new PaginationPage(true);
		}else{
			this.nextPage = new PaginationPage(url, current + 1, page.getSize(), false);
		}
	}
	
	public PaginationPage[] getNavPages() {
		return navPages;
	}
	public PaginationPage getPrevPage() {
		return prevPage;
	}
	public PaginationPage getNextPage() {
		return nextPage;
	}
}
