package com.parking.generic;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@MappedSuperclass
@EntityListeners(AuditFieldsListener.class)
public abstract class AbstractPersistable implements Serializable{

	/**
	 * @author Guddu
	 */
	private static final long serialVersionUID = -5447344137652935700L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="row_id", nullable=false)
	private Long rowId;
	
	@Column(name="created_on", nullable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
	
	@Column(name="last_modified_on", nullable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date modified;
	
	@Column(name="deleted", nullable=false)
	private Boolean deleted;
	
	public Long getRowId() {
		return rowId;
	}
	public void setRowId(Long rowId) {
		this.rowId = rowId;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getModified() {
		return modified;
	}
	public void setModified(Date modified) {
		this.modified = modified;
	}
	public Boolean getDeleted() {
		return deleted;
	}
	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}

		if (this == obj) {
			return true;
		}

		if (!getClass().equals(obj.getClass())) {
			return false;
		}

		AbstractPersistable rhs = (AbstractPersistable) obj;
		return this.rowId == null ? false : this.rowId.equals(rhs.rowId);
	}
}
