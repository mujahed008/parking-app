package com.parking.generic;

import java.util.Date;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

public class AuditFieldsListener {

	@PrePersist 
	public void onPrePersist(Object o) {
		AbstractPersistable entity = (AbstractPersistable) o;
		entity.setDeleted(false);
		entity.setCreated(new Date());
		entity.setModified(new Date());
	}
	
	@PreUpdate 
	public void onPreUpdate(Object o) {
		AbstractPersistable entity = (AbstractPersistable) o;
		entity.setModified(new Date());
	}
}