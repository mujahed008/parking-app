package com.parking.generic;

public class PaginationPage {
	
	private String url;
	private Boolean isActive;
	private Boolean disabled;
	private String page;
	private Integer size;
	
	public PaginationPage(String page){		
		this.page = page;
		this.isActive = false;
	}
	
	public PaginationPage(Boolean disabled){		
		this.disabled = disabled;
	}
	
	public PaginationPage(String url, Integer page, Integer size, Boolean isActive){
		if(!isActive)
		this.url = url + "?page=" + page + "&size=" + size;
		this.page = String.valueOf(page);
		this.size = size;
		this.isActive = isActive;
	}	
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}

	public Boolean getDisabled() {
		return disabled;
	}

	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}
}
