package com.parking.commons;

import org.apache.commons.lang3.StringUtils;

public class Utils {
	
	private Utils(){}
	
	public static String blankIfEmpty(String toCheck){
		return StringUtils.isEmpty(toCheck) ? StringUtils.EMPTY : toCheck;
	};
	
	public static String likeStartsWith(String searchText){
		return AppConstants.PERC + searchText;
	}
	
	public static String likeEndsWith(String searchText){
		return searchText + AppConstants.PERC;
	}
	
	public static String[] splitTypeMedicineName(String medicine){
		String type = null;
		if(medicine.indexOf("Tab. ") == 0){
			type = "Tab. ";
		}else if(medicine.indexOf("Cap. ") == 0){
			type = "Cap. ";
		}else if(medicine.indexOf("Syp. ") == 0){
			type = "Syp. ";
		}else if(medicine.indexOf("Inj. ") == 0){
			type = "Inj. ";
		}else if(medicine.indexOf("Iv. ") == 0){
			type = "Iv. ";
		}
		if(StringUtils.isNotEmpty(type)){
			medicine = medicine.replace(type, StringUtils.EMPTY);
		}
		return new String[] {type, medicine};
	};

}
