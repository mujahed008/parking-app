package com.parking.commons;

public final class AppConstants {

	public static final String _ROLE_SA = "ROLE_SA";
	public static final String _ROLE_ADMIN = "ROLE_ADMIN";
	public static final String _ROLE_DOCTOR = "ROLE_DOCTOR";
	public static final String _ROLE_ASISTANT = "ROLE_ASISTANT";
	public static final String _ROLE_PHARMACY = "ROLE_PHARMACY";
	
	public static final String[] _SIGNUP_ROLES = { _ROLE_ADMIN, _ROLE_DOCTOR };
	
	
	public static final String _PAGE = "1";
	public static final String _SIZE = "15";
	public static final String _MODIFIED = "-modified";
	
	
	public static final String ENTITY_ID_REPO_LIST_URL = "entityIdRepository/list";
	public static final String PATIENT_LIST_URL = "patient/list";
	public static final String LIST_OF_VALUE_LIST_URL = "listOfValue/list";
	
	
	public static final String PARENT = "PARENT";
	
	public static final String PERC = "%";
	
	
	public static final String RX_TEMPLATE = "/prescription/rx-template";
}
