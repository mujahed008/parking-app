package com.parking.pojo;

import java.util.List;

public class DeleteDto {
	
	private List<Long> toDelete;

	public List<Long> getToDelete() {
		return toDelete;
	}
	public void setToDelete(List<Long> toDelete) {
		this.toDelete = toDelete;
	}
}
