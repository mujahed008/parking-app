package com.parking.pojo;

import java.util.List;

import com.parking.generic.Pagination;
import com.parking.model.ListOfValue;

public class ListOfValuePaginatedList {

	private Pagination<ListOfValue> pagination;
	private List<ListOfValueDto> listOfValueDtoList;
	
	public Pagination<ListOfValue> getPagination() {
		return pagination;
	}
	public void setPagination(Pagination<ListOfValue> pagination) {
		this.pagination = pagination;
	}
	public List<ListOfValueDto> getListOfValueDtoList() {
		return listOfValueDtoList;
	}
	public void setListOfValueDtoList(List<ListOfValueDto> listOfValueDtoList) {
		this.listOfValueDtoList = listOfValueDtoList;
	}
}
