package com.parking.pojo;

import java.util.List;

import com.parking.generic.Pagination;
import com.parking.model.EntityIdRepository;

public class EntityIdRepositoryPaginatedList {

	private Pagination<EntityIdRepository> pagination;
	private List<EntityIdRepositoryDto> entityDtoList;
	
	public Pagination<EntityIdRepository> getPagination() {
		return pagination;
	}
	public void setPagination(Pagination<EntityIdRepository> pagination) {
		this.pagination = pagination;
	}
	public List<EntityIdRepositoryDto> getEntityDtoList() {
		return entityDtoList;
	}
	public void setEntityDtoList(List<EntityIdRepositoryDto> entityDtoList) {
		this.entityDtoList = entityDtoList;
	}
}
