package com.parking.pojo;

import java.util.Date;

public class DosageDto {

	private Long rowId;
	private Date created;
	private Date modified;
	private Boolean deleted;
	private Long applicationId;
	private Long srNo;
	private String medicineName;
	private String strength;
	private String generic;
	private String schedule;
	private String qty;
	private String splRemarks;
	private String sakadi;
	private String dopari;
	private String ratri;
	public Long getRowId() {
		return rowId;
	}
	public void setRowId(Long rowId) {
		this.rowId = rowId;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getModified() {
		return modified;
	}
	public void setModified(Date modified) {
		this.modified = modified;
	}
	public Boolean getDeleted() {
		return deleted;
	}
	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
	public Long getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}
	public Long getSrNo() {
		return srNo;
	}
	public void setSrNo(Long srNo) {
		this.srNo = srNo;
	}
	public String getMedicineName() {
		return medicineName;
	}
	public void setMedicineName(String medicineName) {
		this.medicineName = medicineName;
	}
	public String getStrength() {
		return strength;
	}
	public void setStrength(String strength) {
		this.strength = strength;
	}
	public String getGeneric() {
		return generic;
	}
	public void setGeneric(String generic) {
		this.generic = generic;
	}
	public String getSchedule() {
		return schedule;
	}
	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getSplRemarks() {
		return splRemarks;
	}
	public void setSplRemarks(String splRemarks) {
		this.splRemarks = splRemarks;
	}
	public String getSakadi() {
		return sakadi;
	}
	public void setSakadi(String sakadi) {
		this.sakadi = sakadi;
	}
	public String getDopari() {
		return dopari;
	}
	public void setDopari(String dopari) {
		this.dopari = dopari;
	}
	public String getRatri() {
		return ratri;
	}
	public void setRatri(String ratri) {
		this.ratri = ratri;
	}
}
