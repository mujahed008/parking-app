package com.parking.pojo;

public class AuthResponse {

	private String name;
	private String authToken;
	public String getName() {
		return name;
	}
	public String getAuthToken() {
		return authToken;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
}
