package com.parking.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import com.parking.dao.EntityIdRepositoryDao;
import com.parking.forms.EntityIdRepositoryForm;
import com.parking.model.EntityIdRepository;
import com.parking.security.UserDetailsImpl;

public class EntityNameValidator implements ConstraintValidator<EntityName, EntityIdRepositoryForm> {

	@Autowired
	private EntityIdRepositoryDao entityIdRepositoryDao;
	
	@Override
	public void initialize(EntityName entityName) {
		
	}

	@Override
	public boolean isValid(EntityIdRepositoryForm form, ConstraintValidatorContext ctx) {
		
		UserDetailsImpl user = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Long applicationId = user.getUser().getApplication().getRowId();
		
		EntityIdRepository entity = null;
		if(form.getRowId() == null){
			entity = entityIdRepositoryDao.getEntityIdRepository(form.getEntityName(), applicationId);
		}else{
			EntityIdRepository dbEntity = entityIdRepositoryDao.getEntityIdRepository(form.getRowId(), applicationId);
			if(form.getEntityName().equals(dbEntity.getEntityName())){
				return true;
			}else{
				entity = entityIdRepositoryDao.getEntityIdRepository(form.getEntityName(), applicationId);
			}
		}
		
		if(entity != null){
			return false;
		}
		return true;
	}

}
