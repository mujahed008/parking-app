package com.parking.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.parking.model.User;
import com.parking.repository.UserRepository;

public class UsernameValidator implements ConstraintValidator<Username, String> {

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public void initialize(Username arg0) {
		
	}

	@Override
	public boolean isValid(String username, ConstraintValidatorContext arg1) {
		User user = userRepository.findByUsername(username);
		if(user != null)
			return false;
		else
			return true;
	}
}
