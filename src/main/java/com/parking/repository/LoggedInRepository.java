package com.parking.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.parking.model.LoggedIn;

@Repository
public interface LoggedInRepository extends JpaRepository<LoggedIn, Long>, JpaSpecificationExecutor<LoggedIn> {

	public LoggedIn findBySeries(String series);
	
	public LoggedIn findByUsername(String username);
}
