package com.parking.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.parking.model.Application;
import com.parking.model.EntityIdRepository;

@Repository
public interface EntityIdRepositoryRepository extends JpaRepository<EntityIdRepository, Long>, JpaSpecificationExecutor<EntityIdRepository> {

	public EntityIdRepository findByEntityName(String entityName);
	
	public EntityIdRepository findOneByEntityNameAndApplication(String entityName, Application application);
}
