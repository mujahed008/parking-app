package com.parking.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.parking.model.Application;
import com.parking.model.ListOfValue;

@Repository
public interface ListOfValueRepository extends JpaRepository<ListOfValue, Long>, JpaSpecificationExecutor<ListOfValue> {

	public ListOfValue findOneByRowIdAndApplication(Long rowId, Application application);
}
