package com.parking.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import com.parking.generic.AbstractPersistable;

@Entity
@Table(name="role")
@Where(clause="deleted=false")
public class Role extends AbstractPersistable{


	/**
	 * @author Guddu
	 */
	private static final long serialVersionUID = -4191662977973547918L;
	
	@Column(name="role", nullable=false)
	private String role;
	
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
}
