package com.parking.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import com.parking.generic.AbstractPersistable;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="list_of_value")
@Where(clause="deleted=false")
public class ListOfValue extends AbstractPersistable {

	/**
	 * @author Guddu
	 */
	private static final long serialVersionUID = -6714007257341251034L;
	
	@Column(name="display", nullable=false)
	private String display;
	
	@Column(name="val", nullable=false)
	private String val;
	
	@Column(name="type", nullable=false)
	private String type;
	
	@Column(name="parent_type", nullable=false)	
	private String parentType;
	
	@Column(name="sequence_no", nullable=true)
	private Long sequenceNo;
	
	@Column(name="deletable", nullable=false)
	private Boolean deletable = true;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
	@JoinColumn(name="application_id")
	@JsonIgnore
	private Application application;
	
	public String getDisplay() {
		return display;
	}
	public void setDisplay(String display) {
		this.display = display;
	}
	public String getVal() {
		return val;
	}
	public void setVal(String val) {
		this.val = val;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getParentType() {
		return parentType;
	}
	public void setParentType(String parentType) {
		this.parentType = parentType;
	}
	public Long getSequenceNo() {
		return sequenceNo;
	}
	public void setSequenceNo(Long sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	public Boolean getDeletable() {
		return deletable;
	}
	public void setDeletable(Boolean deletable) {
		this.deletable = deletable;
	}
	public Application getApplication() {
		return application;
	}
	public void setApplication(Application application) {
		this.application = application;
	}
}
