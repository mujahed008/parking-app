package com.parking.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import com.parking.generic.AbstractPersistable;

@Entity
@Table(name="logged_in")
@Where(clause="deleted=false")
public class LoggedIn extends AbstractPersistable{

	/**
	 * @author Guddu
	 */
	private static final long serialVersionUID = 8368930948346520807L;
	
	@Column(name="username", nullable=true)
	private String username;
	
	@Column(name="token", nullable=true)
	private String token;
	
	@Column(name="series", nullable=true)
	private String series;
	
	@Column(name="last_used", nullable=true)
	private Date date;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getSeries() {
		return series;
	}
	public void setSeries(String series) {
		this.series = series;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

}
