package com.parking.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import com.parking.generic.AbstractPersistable;

@Entity
@Table(name="user")
@Where(clause="deleted=false")
public class User extends AbstractPersistable{

	/**
	 * @author Guddu
	 */
	private static final long serialVersionUID = -626149103773938857L;
	
	@Column(name="name", nullable=true)
	private String name;
	
	@Column(name="email", nullable=true)
	private String email;
	
	@Column(name="username", nullable=true)
	private String username;
	
	@Column(name="password", nullable=true)
	private String password;
	
	@ManyToMany(cascade=CascadeType.PERSIST)
	@JoinTable(
	        name = "user_role",
    		joinColumns = {@JoinColumn(name = "user_id", referencedColumnName="row_id")},
	        inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName="row_id")}
	)
	private Set<Role> roles;
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinTable(
	        name = "user__application",
    		joinColumns = {@JoinColumn(name = "user_id", referencedColumnName="row_id")},
	        inverseJoinColumns = {@JoinColumn(name = "application_id", referencedColumnName="row_id")}
	)
	private Application application;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Set<Role> getRoles() {
		return roles;
	}
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	public Application getApplication() {
		return application;
	}
	public void setApplication(Application application) {
		this.application = application;
	}
}
