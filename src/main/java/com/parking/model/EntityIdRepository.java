package com.parking.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import com.parking.generic.AbstractPersistable;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="entity_id_repository")
@Where(clause="deleted=false")
public class EntityIdRepository extends AbstractPersistable{

	/**
	 * @author Guddu
	 */
	private static final long serialVersionUID = -8724025053163588196L;
	
	@Column(name="prefix", nullable=true)
	private String prefix;
	
	@Column(name="suffix", nullable=true)
	private String suffix;
	
	@Column(name="last_sr_no", nullable=false)
	private Long lastSrNo;
	
	@Column(name="entity_name", nullable=true)
	private String entityName;
	
	@Column(name="description", nullable=true)
	private String discription;
	
	@Column(name="key_", nullable=true)
	private String key;
	
	@Column(name="value_", nullable=true)
	private String value;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
	@JoinColumn(name="application_id")
	@JsonIgnore
	private Application application;
	
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	public Long getLastSrNo() {
		return lastSrNo;
	}
	public void setLastSrNo(Long lastSrNo) {
		this.lastSrNo = lastSrNo;
	}
	public String getEntityName() {
		return entityName;
	}
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
	public String getDiscription() {
		return discription;
	}
	public void setDiscription(String discription) {
		this.discription = discription;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Application getApplication() {
		return application;
	}
	public void setApplication(Application application) {
		this.application = application;
	}
}
