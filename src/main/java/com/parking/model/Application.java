package com.parking.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import com.parking.generic.AbstractPersistable;

@Entity
@Table(name="application")
@Where(clause="deleted=false")
public class Application extends AbstractPersistable{

	/**
	 * @author Guddu
	 */
	private static final long serialVersionUID = -1396977499274019866L;
	
	@Column(name="app_name", nullable=true)
	private String appName;
	
	@Column(name="clinic_name", nullable=true)
	private String clinicName;
	
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public String getClinicName() {
		return clinicName;
	}
	public void setClinicName(String clinicName) {
		this.clinicName = clinicName;
	}
}
