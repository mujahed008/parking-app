package com.parking.forms;

import com.parking.validator.EntityName;

@EntityName
public class EntityIdRepositoryForm {

	private Long rowId;
	private String prefix;
	private String suffix;
	private Long lastSrNo;
	private String entityName;
	private String discription;
	private String key;
	private String value;
	
	public Long getRowId() {
		return rowId;
	}
	public void setRowId(Long rowId) {
		this.rowId = rowId;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	public Long getLastSrNo() {
		return lastSrNo;
	}
	public void setLastSrNo(Long lastSrNo) {
		this.lastSrNo = lastSrNo;
	}
	
	public String getEntityName() {
		return entityName;
	}
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
	public String getDiscription() {
		return discription;
	}
	public void setDiscription(String discription) {
		this.discription = discription;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
